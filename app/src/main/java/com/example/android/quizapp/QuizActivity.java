package com.example.android.quizapp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class QuizActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);
    }

    //Add button to action bar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.view_answers, menu);
        return true;
    }

    //Show toast after finishing quiz
    public void showToast(double totalScore) {
        //check if name empty at set name
        EditText editText = (EditText) findViewById(R.id.name);
        String name = editText.getText().toString();
        if (name.isEmpty()) {
            name = "Mr.Incognito";
        }

        if (totalScore == 9) {
            Toast.makeText(this, "Woohoo! " + name + ", you answered " +
                    "everything right! Congrats! \nYour score: " + totalScore + "/9", Toast.LENGTH_LONG).show();
        } else if (totalScore == 0) {
            Toast.makeText(this, "Booooo! " + name + ", you " +
                    "answered everything wrong! You make me sad... \nYour score: " + totalScore + "/9", Toast.LENGTH_LONG).show();
        } else if (totalScore > 0 && totalScore < 4.5) {
            Toast.makeText(this, "Well... " + name + ", your " +
                    "right answers are less than 50%! Not so bad! \nYour score: " + totalScore + "/9", Toast.LENGTH_LONG).show();
        } else if (totalScore > 4.5 && totalScore < 9) {
            Toast.makeText(this, "Well... " + name + ", your " +
                    "right answers are more than 50%! Good job! \nYour score: " + totalScore + "/9", Toast.LENGTH_LONG).show();
        } else if (totalScore == 4.5) {
            Toast.makeText(this, "Wow! " + name + ", your " +
                    "right answers are right in the middle! Good job! \nYour score: " + totalScore + "/9", Toast.LENGTH_LONG).show();
        }
    }

    //Show answers after pushing button in action bar
    public void showAnswers(MenuItem item) {
//Clear all radiobuttons checks
        ((RadioGroup) findViewById(R.id.ten_radio_group)).clearCheck();
        ((RadioGroup) findViewById(R.id.nine_radio_group)).clearCheck();
        ((RadioGroup) findViewById(R.id.eight_radio_group)).clearCheck();
        ((RadioGroup) findViewById(R.id.seven_radio_group)).clearCheck();
        ((RadioGroup) findViewById(R.id.six_radio_group)).clearCheck();
        ((RadioGroup) findViewById(R.id.five_radio_group)).clearCheck();
//Clear all unneded checkboxes
        ((CheckBox) findViewById(R.id.two_two)).setChecked(false);
        ((CheckBox) findViewById(R.id.two_three)).setChecked(false);
        ((CheckBox) findViewById(R.id.three_one)).setChecked(false);
        ((CheckBox) findViewById(R.id.three_three)).setChecked(false);
        ((CheckBox) findViewById(R.id.four_three)).setChecked(false);
        ((CheckBox) findViewById(R.id.four_four)).setChecked(false);
//Set needed checkboxes
        ((CheckBox) findViewById(R.id.two_one)).setChecked(true);
        ((CheckBox) findViewById(R.id.two_four)).setChecked(true);
        ((CheckBox) findViewById(R.id.three_two)).setChecked(true);
        ((CheckBox) findViewById(R.id.three_four)).setChecked(true);
        ((CheckBox) findViewById(R.id.four_one)).setChecked(true);
        ((CheckBox) findViewById(R.id.four_two)).setChecked(true);
//set needed radiobuttons
        ((RadioButton) findViewById(R.id.five_four)).setChecked(true);
        ((RadioButton) findViewById(R.id.six_three)).setChecked(true);
        ((RadioButton) findViewById(R.id.seven_one)).setChecked(true);
        ((RadioButton) findViewById(R.id.eight_two)).setChecked(true);
        ((RadioButton) findViewById(R.id.nine_four)).setChecked(true);
        ((RadioButton) findViewById(R.id.ten_one)).setChecked(true);
//show toast
        Toast.makeText(this, "Here's the right answers!", Toast.LENGTH_LONG).show();
    }

    //Finish quiz and calculate score
    public void finishQuiz(View view) {
        double firstQuest = 0;
        double secondQuest = 0;
        double thirdQuest = 0;
        double totalScore = 0;

        //Check second question answers

        if (((CheckBox) findViewById(R.id.two_one)).isChecked()) {
            firstQuest += 0.5;
        }
        if (((CheckBox) findViewById(R.id.two_two)).isChecked()) {
            firstQuest -= 0.5;
        }
        if (((CheckBox) findViewById(R.id.two_three)).isChecked()) {
            firstQuest -= 0.5;
        }
        if (((CheckBox) findViewById(R.id.two_four)).isChecked()) {
            firstQuest += 0.5;
        }

        //Check if score negative or positive
        if (firstQuest < 0) {
            firstQuest = 0;
        } else {
            totalScore += firstQuest;
        }

        //Check third question answers
        if (((CheckBox) findViewById(R.id.three_one)).isChecked()) {
            secondQuest -= 0.5;
        }
        if (((CheckBox) findViewById(R.id.three_two)).isChecked()) {
            secondQuest += 0.5;
        }
        if (((CheckBox) findViewById(R.id.three_three)).isChecked()) {
            secondQuest -= 0.5;
        }
        if (((CheckBox) findViewById(R.id.three_four)).isChecked()) {
            secondQuest += 0.5;
        }

        //Check if score negative or positive
        if (secondQuest < 0) {
            secondQuest = 0;
        } else {
            totalScore += secondQuest;
        }

        //Check fourth answers question
        if (((CheckBox) findViewById(R.id.four_one)).isChecked()) {
            thirdQuest += 0.5;
        }
        if (((CheckBox) findViewById(R.id.four_two)).isChecked()) {
            thirdQuest += 0.5;
        }
        if (((CheckBox) findViewById(R.id.four_three)).isChecked()) {
            thirdQuest -= 0.5;
        }
        if (((CheckBox) findViewById(R.id.four_four)).isChecked()) {
            thirdQuest -= 0.5;
        }

        //Check if score negative or positive
        if (thirdQuest < 0) {
            thirdQuest = 0;
        } else {
            totalScore += thirdQuest;
        }

        //Check other questions
        if (((RadioButton) findViewById(R.id.five_four)).isChecked()) {
            totalScore += 1;
        }
        if (((RadioButton) findViewById(R.id.six_three)).isChecked()) {
            totalScore += 1;
        }
        if (((RadioButton) findViewById(R.id.seven_one)).isChecked()) {
            totalScore += 1;
        }
        if (((RadioButton) findViewById(R.id.eight_two)).isChecked()) {
            totalScore += 1;
        }
        if (((RadioButton) findViewById(R.id.nine_four)).isChecked()) {
            totalScore += 1;
        }
        if (((RadioButton) findViewById(R.id.ten_one)).isChecked()) {
            totalScore += 1;
        }

        //Show toast
        showToast(totalScore);
    }
}
